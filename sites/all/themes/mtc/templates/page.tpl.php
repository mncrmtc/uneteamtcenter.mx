<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
  <?php print $messages; ?>
  <!-- <a target="_blank" href="http://mtcchat.com.mx/mtclive/chat.php"style="position:fixed; bottom:-3px; right:15px; z-index:10000" class="chat btn btn-danger"><span class="chat-icon"></span>¿Dudas? Chatea con uno de nuestros asesores</a> -->
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-default navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="http://www.mtcenter.com.mx"><img src="<?php echo $base_path;?>sites/all/themes/mtc/images/logo.png" alt=""></a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav pull-right">
              <li style="margin-bottom:10px; float:right;">
                <h4>Compártenos:</h4>
                  <span class='st_facebook_large' displayText='Facebook'></span>
                  <span class='st_twitter_large' displayText='Tweet'></span>
                  <span class='st_googleplus_large' displayText='Google +'></span>
                  <span class='st_email_large' displayText='Email'></span>
                </li>
                
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
       <div class="contacto col-md-3">
            <?php print render($page['sidebar_first']); ?>
          </div>
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="<?php echo $base_path;?>sites/all/themes/mtc/images/jumbotron/jumbo1.jpg" alt="First slide">
          <div class="container">

          </div>
        </div>

        <div class="item">
          <img src="<?php echo $base_path;?>sites/all/themes/mtc/images/jumbotron/jumbo2.jpg" alt="Second slide">
          <div class="container">
            
          </div>
        </div>

         <div class="item">
          <img src="<?php echo $base_path;?>sites/all/themes/mtc/images/jumbotron/jumbo3.jpg" alt="Second slide">
          <div class="container">
            
          </div>
        </div>
      

      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>

    </div><!-- /.carousel -->

<div class="col-md-3 main-content">
      <?php print render($page['content']); ?>
</div>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
      <h2 style="text-align:center; margin:35px 0" class="featurette-heading">Beneficios</h2>
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo $base_path;?>sites/all/themes/mtc/images/1.png" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Asistencia</h2>
          <p>Servicio de asistencia los 365 días del año.</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo $base_path;?>sites/all/themes/mtc/images/2.png" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Crédito</h2>
          <p>Crédito a tu negocio, para que nunca dejes de vender.</p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="<?php echo $base_path;?>sites/all/themes/mtc/images/3.png" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Accesibilidad</h2>
          <p>Usalo desde tu computadora o smartphone.</p>
        </div><!-- /.col-lg-4 -->
        <p class="download">
          <a href="http://www.mtcenter.com.mx/instala.php">
            <img src="<?php echo $base_path;?>sites/all/themes/mtc/images/pwin.png" alt="">
          </a>
          <a href="https://play.google.com/store/apps/details?id=air.com.mtcenter.movil">
            <img src="<?php echo $base_path;?>sites/all/themes/mtc/images/pandroid.png" alt="">
          </a>
          <a href="https://search.itunes.apple.com/WebObjects/MZContentLink.woa/wa/link?path=apps/mtcenter">
            <img src="<?php echo $base_path;?>sites/all/themes/mtc/images/pios.png" alt="">
          </a>
        </p>
      </div><!-- /.row -->


    </div><!-- /.container -->

      <!-- START THE FEATURETTES -->

      <div class="row featurette proceso">
        <div class="container">
        <div class="col-md-6">
          <!--   <h2 class="featurette-heading">Proceso</h2> -->
          <img src="<?php echo $base_path;?>sites/all/themes/mtc/images/proceso.png" alt="">
          </div>
        <div style="text-align:right" class="col-md-6">
        <span class="glyphicon glyphicon-earphone phone"></span>
              <?php print render($page['sidebar_second']); ?>
                
        </div>
          
        </div>
      </div>

      <!-- /END THE FEATURETTES -->

            <!-- START THE FEATURETTES -->

      <div class="row featurette testimonios">
        <div class="container">
          
            <h2 class="featurette-heading">Testimonios</h2>
            <div class="row">
              <div class="col-md-4 testimonio">
              <p style="text-align:center"><img  style="width:40%" src="<?php echo $base_path;?>sites/all/themes/mtc/images/testimonios/farmapronto.png" alt=""></p>
              <p style="clear:both">“Recomiendo a la empresa MTCenter, quienes se han desempeñado como nuestro principal proveedor de Servicios Electrónicos, principalmente en recargas electrónicas, pago de servicios, pago de remesas, pines y prepago. <br><br>
              Productos que le han dado gran satisfacción a nuestra empresa y a nuestros clientes, gracias a éstos, se ha incrementado el flujo de gente en nuestros puntos de venta y por ende nuestras ventas en farmacia; así mismo, nos genera ingresos adicionales por las comisiones recibidas en todos los servicios. <br><br>
              Desde el inicio de operaciones hasta la fecha todos sus ejecutivos han demostrado profesionalismo, rapidez y eficiencia, ofreciéndonos servicios de calidad."  <br>
              <br>
              <span class="autor">
              –Miguel González <br>
              Administrador Único <br>
              Farmacias Farmapronto.</span></p>

              </div>
              <div class="col-md-4 testimonio">
              <p style="text-align:center"><img  style="width:40%" src="<?php echo $base_path;?>sites/all/themes/mtc/images/testimonios/icma-logo.png" alt=""></p>
              <p style="clear:both">“En el año 2006, inicié una relación comercial con la empresa MTCenter, que instaló la plataforma para la venta de tiempo aire, así como para el pago de diversos servicios, como CFE, TELMEX, INFONAVIT, CABLEMAS, AXTEL, SKY, entre  muchos otros. Lo cual coadyuvó al crecimiento de mi negocio al poder ofrecer una gama más amplia de servicios a mis clientes y por ende se vio reflejado en mis finanzas. <br><br>

              Considero que empresas de esa naturaleza fomentan las alianzas económicas en el pro del crecimiento del país.”<br><br>

              <span class="autor">–Marcelino Cortés <br>
              Ciber y Comercializadora ICMA</span> </p>
             
              </div>
              <div class="col-md-4 testimonio">
              <p style="text-align:center"><img  style="width:40%" src="<?php echo $base_path;?>sites/all/themes/mtc/images/testimonios/sanjudas.png" alt=""></p>
              <p style="clear:both">“Gracias al servicio de venta de tiempo aire hemos podido incrementar las ventas, así mismo ha servido para atraer gente a mi negocio.”
              <span class="autor">–CP. Susana López Altamirano<br>
              Farmacia San Judas Tadeo</span></p>
              
              </div>

        
        </div>
      </div>

      <!-- /END THE FEATURETTES -->
</div>
 
      <!-- FOOTER -->
      <footer id="footer">
        <div class="container">
          <div style="overflow:hidden; display:block; width:212px; margin:10px auto;" class="social">
          <h4 style="text-align:center">Síguenos en:</h4>
            <a style="margin: 0 2px; display:block; float:left;" href="https://www.facebook.com/MTCenterOficial"><img src="<?php echo $base_path;?>sites/all/themes/mtc/images/social/fb.png" alt=""></a>
            <a style="margin: 0 2px; display:block; float:left;" href="https://twitter.com/MTCenterOficial"><img src="<?php echo $base_path;?>sites/all/themes/mtc/images/social/tw.png" alt=""></a>
            <a style="margin: 0 2px; display:block; float:left;" href="https://plus.google.com/+mtcenter/posts"><img src="<?php echo $base_path;?>sites/all/themes/mtc/images/social/gplus.png" alt=""></a>
            <a style="margin: 0 2px; display:block; float:left;" href="https://www.youtube.com/channel/UCcSTLjQwHy_FhrUdNS_wyCw"><img src="<?php echo $base_path;?>sites/all/themes/mtc/images/social/yt.png" alt=""></a>
          </div>
          <p style="text-align:center">MTCenter © 2014 | Lada sin costo: 01 800 350 3503 | 01 (777) 311 3066 | 01 (777) 177 0363 | <a href="http://www.mtcenter.com.mx">www.mtcenter.com.mx</a> | <a class="underline" href="http://www.mtcenter.com.mx/privacidad/">Aviso de privacidad</a></p>
        </div>
      </footer>